import java.util.Scanner;

public class InputTest {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Enter 2 integer numbers:");

        int x = in.nextInt();
        int y = in.nextInt();

        int z = x + y;

        System.out.println(x + " + " + y + " = " + z);
    }

}
