import java.io.FileWriter;

// Creates a CSV file and saves the data from an array of books to that file
@SuppressWarnings("ALL")
public class BookCreator
{
    public static void main(String[] args)
    {
        // sample books to save in out CSV File
        Book[] newBooks = new Book[3];
        newBooks[0] = new Book("The Hobbit", "J. R. R. Tolkien", "21 September 1937");
        newBooks[1] = new Book("Nineteen Eighty-Four", "George Orwell", "8 June 1949");
        newBooks[2] = new Book("Pride and Prejudice", "Jane Austen", "28 January 1813");

        try
        {
            FileWriter csvWriter = new FileWriter("data/books.csv");

            // create "header" row first
            csvWriter.append("Title");
            csvWriter.append(",");               // comma separators
            csvWriter.append("Author");
            csvWriter.append(",");
            csvWriter.append("Date");
            csvWriter.append("\n");             // new line character

            // create a new line in the file for each Book

            for (int i = 0; i < newBooks.length; i++)
            {
                Book newBook = newBooks[i];

                csvWriter.append(newBook.getTitle());
                csvWriter.append(",");
                csvWriter.append(newBook.getAuthor());
                csvWriter.append(",");
                csvWriter.append(newBook.getDate());
                csvWriter.append("\n");
            }

            csvWriter.flush();
            csvWriter.close();



            // remember to flush and close when finished
        }
        catch (Exception e)         // Catch ANY Exception
        {
            System.out.println(e.getMessage());     // Print the Exception message
            System.out.println(e.getStackTrace());  // Print the Stack Trace
            System.exit(1);                         // (Optionally) Exit (with code 1 to indicate an error has occurred)
        }
    }
}
