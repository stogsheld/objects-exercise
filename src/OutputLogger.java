import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

// this code should write three lines to a file called default.log 
// (in the directory above where this Java file is)

public class OutputLogger
{
    public static void main(String[] args)
    {
        Logger logger = null;       // define our logger in scope, so we can use it after the try statement

        try
        {
            boolean append = true;      // specify whether to append to the existing file (true) or overwrite (false)

            FileHandler handler = new FileHandler("default.log", append);
            handler.setFormatter(new SimpleFormatter());        // use a SimpleFormatter to print in plain text rather than XML

            logger = Logger.getLogger("test_output_logger");
            logger.addHandler(handler);
        }
        catch (Exception e)                 // Catch ANY Exception that is thrown
        {
            System.out.println(e.getMessage());     // Print the Exception message
            System.out.println(e.getStackTrace());  // Print the Stack Trace
            System.exit(1);                         // (Optional) Exit (with code 1 to indicate an error has occurred)
        }

        logger.info("info message");            // write an info message
        logger.warning("warning message");      // write an warning
        logger.severe("severe message");        // write an error message
    }

}
