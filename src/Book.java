// Class to store Book information
public class Book
{
    // attributes
    private String title;
    private String author;
    private String date;

    // constructor
    public Book(String title, String author, String date)
    {
        this.title = title;
        this.author = author;
        this.date = date;
    }

    // methods go here!

    // Getters to check that the books will print out
    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getDate() {
        return date;
    }
}


