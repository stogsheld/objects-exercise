// import statements
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

@SuppressWarnings("ALL")
// Code for loading Book info from a CSV File
public class BookLoader
{
    public static void main(String[] args)
    {
        try
        {
            String csvFile = "data/books.csv";                                                          // CSV filename

            FileReader fileReader = new FileReader(csvFile);                                        // File loading Objects
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            Book[] books = new Book[10];                                                        // Array for storing the Book objects.

            String nextLine = bufferedReader.readLine();                                        // get the first line of the file (ignore first line - column headers)

            for (int i = 0; i < books.length; i++)
            {
                nextLine = bufferedReader.readLine();                                           // get the next line

                if (nextLine != null)
                {
                    String[] strings = nextLine.split(",");                                     // split the string wherever commas occur

                    String title = strings[0];                                                      // access the array of Strings returned by split method
                    String author = strings[1];
                    String date = strings[2];

                    books[i] = new Book(title, author, date);                                   // create new book and add to array

                    // Printing out book details to verify data
                    System.out.println(books[i].getTitle() + ", " + books[i].getAuthor() + ", " + books[i].getDate());
                }
            }
        }
        catch (Exception e)                 // Catch ANY Exception that is thrown
        {
            System.out.println(e.getMessage());     // Print the Exception message
            System.out.println(e.getStackTrace());  // Print the Stack Trace
            System.exit(1);                         // (Optionally) Exit (with code 1 to indicate an error has occurred)
        }
    }
}
